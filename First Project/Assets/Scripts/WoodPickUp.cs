﻿using UnityEngine;
using System.Collections;

public class WoodPickUp : MonoBehaviour {
	public Transform player;
	public float throwForce = 10;
	bool beingCarried = false;
	public GameObject arrow;
	public  float distance = 10.0f;
	private PlayerControl playerCtrl;
	public GameObject hower;
	public  bool CanPickUp=false;
	public static bool PickUp=false;


	void Start(){

		hower.SetActive (false);
		arrow.SetActive (false);
		playerCtrl = player.transform.root.GetComponent<PlayerControl>();
	}

	void OnMouseEnter(){
		if (distance < 2) {
			hower.SetActive (true);
			hower.transform.position = this.transform.position;
			hower.GetComponent<Rigidbody2D>().isKinematic = true;
			CanPickUp = true;
		}
	}
	void OnMouseExit(){
		hower.SetActive (false);
		CanPickUp = false;
	}

	void Update()
	{
		distance = Vector2.Distance (this.transform.position, player.transform.position);
		if(beingCarried)
		{
			if(Input.GetKeyDown("e"))
			{
				PickUp=false;
				arrow.SetActive (false);
				GetComponent<Rigidbody2D>().isKinematic = false;
				this.transform.parent = null;
				beingCarried = false;
				GetComponent<Rigidbody2D>().AddForce(player.forward * throwForce);
			}
		}
		else
		{
			if(Input.GetMouseButton(0) && distance<2 && CanPickUp==true )
			{
				PickUp=true;
				arrow.SetActive (true);
				GetComponent<Rigidbody2D>().isKinematic = true;
				this.transform.parent = player;
				beingCarried = true;
				if(playerCtrl.facingRight)
					this.transform.position = new Vector2(player.transform.position.x+1.5f, player.transform.position.y+1.5f);
				else
					this.transform.position = new Vector2(player.transform.position.x-1.5f, player.transform.position.y+1.5f);
			}
		}
	}
}