﻿using UnityEngine;
using System.Collections;

public class RockRotate : MonoBehaviour {
	private float baseAngle = 0.0f;
	private PlayerControl playerCtrl;
	public float ang;
	public Transform player;

	void Start(){
		playerCtrl = player.transform.root.GetComponent<PlayerControl>();
	}
	
	void OnMouseDown(){
		//if (PickUpObject.PickUp==true) {
			Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
			pos = Input.mousePosition - pos;
			baseAngle = Mathf.Atan2 (pos.y, pos.x) * Mathf.Rad2Deg;
			baseAngle -= Mathf.Atan2 (transform.right.y, transform.right.x) * Mathf.Rad2Deg;
		//}
	}
	
	void OnMouseDrag(){
		if (RockPickUp.PickUp == true) {
			Vector3 pos = Camera.main.WorldToScreenPoint (transform.position);
			pos = Input.mousePosition - pos;
			if (playerCtrl.facingRight)
				ang = Mathf.Atan2 (pos.y, pos.x) * Mathf.Rad2Deg;
			else
				ang = Mathf.Atan2 (pos.y, pos.x * -1) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.AngleAxis (ang, Vector3.forward);

		}
	}
}